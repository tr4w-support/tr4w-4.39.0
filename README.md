# TR4W #

(TRLOG 4 Windows)

TR4W is an adaption of TRLOG (DOS) written by N6TR.

TR4W was written by UA4WLI and exploits the Windows API in Object Pascal.
It may be compiled using the Delphi 7 compiler. 
You can google 'delphi 7 personal edition' for ways to obtain the compiler.

The easiest way to get the code on your PC is GIT (you can select the main source page down arrow).
However you may also do the more laborious task of copying all directories to your PC.
You may also contact n4af@n4af.net for assistance with getting a clean compile.

The program is used for radio amateur logging - mostly in contests.

The program is licensed to GPL V3 and copyrighted by N6TR (DOS) and UA4WLI (Windows)

Further info is at http://tr4w.net/